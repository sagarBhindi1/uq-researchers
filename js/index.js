$(document).ready(function() {
	// For displaying the thumbnail of the selected image
	$("#profilePic").change(function() {
		readURL(this);
	});

	$("#profilePicButton").on("click", function() {
		document.getElementById('profilePic').click();
	});

	// Disable the submit button until the form isn't valid
	$("#submitForm").addClass("disabled");

	$('.requiredField').on('keyup', function () {
	    var reqlength = $('.requiredField').length;
	    var value = $('.requiredField').filter(function () {
	        return this.value != '';
	    });

	    if (value.length>=0 && (value.length !== reqlength)) {
	        $("#submitForm").addClass("disabled");
	    } else {
	        $("#submitForm").removeClass("disabled");
	    }
	});


	$("#submitForm").on('click', function() {
		// Update the values in the view profile form
    	$("#researcherName").text($("#firstName").val() + " " + $("#lastName").val());
		$("#researcherPosition").text($("#position").val());
		$("#researcherEmail").text($("#email").val());
		$(".researcherBiography").text($("#biography").val());

		// Hide the create profile page
		$("#viewProfileContainer").removeClass("hidden");

    	// Show the researcher profile
		$("#researcherProfileFormContainer").addClass("hidden");
	});
});

// For displaying the preview of the selected image
function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function(e) {
			$('.researcherProfilePic').css('width', "125px");
			$('.researcherProfilePic').css('height', "125px");
			$('.researcherProfilePic').css('background', "url('" + e.target.result + "') no-repeat center center");
			$('.researcherProfilePic').css('background-size', "cover");
			$('.researcherProfilePic').css('background-position', "cener");
		}

		reader.readAsDataURL(input.files[0]);
	}
}